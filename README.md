# SSO-Service

A sample Authentication service implemented using Spring framework .The application is a SingleSignON(SSO) login configured through [onelogin](https://www.onelogin.com/) with SAML protocol.


### Components
  
#### Spring Security SAML
  
  Spring Security SAML Extension allows seamless combination of SAML 2.0 and authentication and federation mechanisms in a single application. All products supporting SAML 2.0 in Identity Provider mode like OneLogin can be used to connect with the extension.
  
  Features
  
  * Supports multiple SAML 2.0 profiles (web single sign-on, web single sign-on holder-of-key, single logout, enhanced client/proxy, etc)
  * IDP and SP initialized single sign-on
  * Automatic service provider metadata generation
  * Metadata loading from files, URLs, file-backed URLs
  * Processing of SAML attributes


#### JSON Web Token (JWT)
  
  JWT is a means of transmitting information between two parties in a compact, verifiable form. The bits of information encoded in the body of a JWT are called claims. The expanded form of the JWT is in a JSON format, so each claim is a key in the JSON object.
  JWTs can be cryptographically signed (making it a JWS) or encrypted (making it a JWE).
  This adds a powerful layer of verifiability to the account of JWTs. The receiver has a high degree of confidence that the JWT has not been tampered with by verifying the signature.
  
  >   JJWT - an easy to use and understandable library for creating and verifying JSON Web Tokens (JWTs) on the JVM is used.
      
### Tech Stack:
* Spring-framework with spring security extending SAML extension.
* [Onelogin](https://www.onelogin.com/) account configured with a SAML Connector app(with attributes).
* Redis-server

### Prerequisites

  * Java 8 or higher
  * Gradle 4.1 or higher 
  * Docker-compose       

#### Onelogin Congfiguration:

Create an SAML Connector app in Onelogin environment and capture the appId and entityId. 
Provide the appId and entityId as environment variables while starting the application.
   
       
      ONELOGIN_APP_ID= 799862 <app id configured in onelogin>
      
      ONELOGIN_ENTITY_ID= AUTH <entity id configured in onelogin app - same as audience>


#### Environment Variables :

The following environment variables needs to be configured in application.yml . 

* AWS_REDIS_URL
* ONELOGIN_APP_ID
* ONELOGIN_ENTITY_ID
* ONELOGIN_SUBDOMAIN
   
### Run the application


      docker-compose up ## Starts the redis server locally.
      gradle bootRun

