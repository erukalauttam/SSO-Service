package com.auth.server;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point of auth-client.
 */

@SpringBootApplication(scanBasePackages = "com.auth")
public class AuthServerApplication {

  /**
   * Auth client entry point.
   * @param args the args
   */
  public static void main(String[] args) {
    SpringApplication.run(AuthServerApplication.class, args);
  }

}
