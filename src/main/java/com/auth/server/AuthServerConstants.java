package com.auth.server;

/**
 * Constants specific to Auth Server.
 *
 * @author kayalvizhi
 */
public class AuthServerConstants {

    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
    public static final String JWT_SESSION_STORE = "JWT_SESSION_STORE";
    public static final String JWT_SECRET = "yeWAgVDfb$!MFn@MCJVN7uqkznHbDLR#";
    public static final String ONELOGIN_COOKIE_NAME = "auth_session";
    public static final String ONELOGIN_METADATA_URL = "https://app.onelogin.com/saml/metadata/";
    public static final String NAMEID_FORMAT = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress";
    public static final String PROTOCOL_REDIS = "redis://";
    public static final String CHAR_COLON = ":";
    public static final String CHAR_COMMA = ",";
    public static final String CHAR_EQUAL = "=";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String AUTHORITIES = "authorities";
    public static final String TOKEN_ID = "tokenId";
    public static final String COMPANY = "company";
    public static final String SESSION_INDEX = "sessionIndex";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String ATTRIBUTES = "attributes";
    public static final String EXPIRATION = "exp";
    public static final String AUTH = "auth-service";

    //Error Codes
    public static final String ERROR_JWT_INVALID = "jwt.invalid.error";
    public static final String ERROR_REDIS = "redis.error";

}
