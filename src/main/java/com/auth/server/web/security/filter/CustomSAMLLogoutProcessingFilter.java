package com.auth.server.web.security.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.RedisHandler;
import com.auth.server.utils.saml.SAMLUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLLogoutProcessingFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;


/**
 * Extends @{@link SAMLLogoutProcessingFilter} to delete user entry from Redis
 * after successful logout.
 */
public class CustomSAMLLogoutProcessingFilter extends SAMLLogoutProcessingFilter {


  /**
   * Constructor defines URL to redirect to after successful logout and handlers.
   *
   * @param logoutSuccessHandler user will be redirected to the url after successful logout
   * @param handlers handlers to invoke after logout
   */
  public CustomSAMLLogoutProcessingFilter(LogoutSuccessHandler logoutSuccessHandler,
                                          LogoutHandler... handlers) {
    super(logoutSuccessHandler, handlers);
  }

  /**
   * Delete Redis entry for the current user and delegate to @{@link SAMLLogoutProcessingFilter} for
   * other post success logout handlers.
   *
   * @param req req
   * @param res res
   * @param chain chain
   * @throws IOException IOException
   * @throws ServletException ServletException
   */
  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    if (requiresLogout(request, response)) {
      JWTAuthToken jwtAuthToken = SAMLUtils.getAuthTokenFromRequest(request);
      RedisHandler
          .deleteEntry(jwtAuthToken.getName(), jwtAuthToken.getTokenId());

      jwtAuthToken = SAMLUtils.buildJWTAuthTokenWithSAMLCredential(SAMLUtils
          .generateSAMLCredential(jwtAuthToken.getName(),
              jwtAuthToken.getSessionIndex(), req.getParameter("appId"), req.getParameter("entityId")));
      SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);

      super.doFilter(req, res, chain);
    } else {
      chain.doFilter(request, response);
    }
  }
}
