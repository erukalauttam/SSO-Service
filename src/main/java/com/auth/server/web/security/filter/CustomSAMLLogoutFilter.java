package com.auth.server.web.security.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.saml.SAMLUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLLogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;


/**
 * Extends {@link SAMLLogoutFilter} to prepare logout request.
 * Adds SamlCredential to the Authentication credential
 * for SAMLLogoutFilter to logout the user.
 */
public class CustomSAMLLogoutFilter extends SAMLLogoutFilter {

  /**
   * Default constructor.
   *
   * @param successUrl url to use after logout in case of local logout
   * @param localHandler handlers to be invoked when local logout is selected
   * @param globalHandlers handlers to be invoked when global logout is selected
   */
  public CustomSAMLLogoutFilter(String successUrl, LogoutHandler[] localHandler,
                                LogoutHandler[] globalHandlers) {
    super(successUrl, localHandler, globalHandlers);
  }

  /**
   * Default constructor.
   *
   * @param logoutSuccessHandler handler to invoke upon successful logout
   * @param localHandler handlers to be invoked when local logout is selected
   * @param globalHandlers handlers to be invoked when global logout is selected
   */
  public CustomSAMLLogoutFilter(LogoutSuccessHandler logoutSuccessHandler,
                                LogoutHandler[] localHandler,
                                LogoutHandler[] globalHandlers) {
    super(logoutSuccessHandler, localHandler, globalHandlers);
  }

  /**
   * Obtain SAMLCredential from redis store and add the Authentication credential and delegate to
   * SAMLLogoutFilter's doFilter.
   *
   * @param request request
   * @param response response
   * @param chain chain
   * @throws IOException IOException
   * @throws ServletException ServletException
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;

    if (requiresLogout(req, res)) {
      JWTAuthToken jwtAuthToken = SAMLUtils.getAuthTokenFromRequest(req);
      if (null == jwtAuthToken) {
        chain.doFilter(request, response);
        return;
      }
      jwtAuthToken = SAMLUtils.buildJWTAuthTokenWithSAMLCredential(SAMLUtils
          .generateSAMLCredential(jwtAuthToken.getName(),
          jwtAuthToken.getSessionIndex(), req.getParameter("appId"), req.getParameter("entityId")));
      SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);
      super.doFilter(request, response, chain);
    } else {
      chain.doFilter(request, response);
    }

  }
}
