package com.auth.server.web.security.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.saml.SAMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.SAMLProcessingFilter;


/**
 * Overrides @{@link SAMLProcessingFilter} to create and set JWT token on
 * successful authentication.
 */
public class CustomSAMLProcessingFilter extends SAMLProcessingFilter {

    private static Logger logger = LoggerFactory.getLogger(CustomSAMLProcessingFilter.class);

    /**
     * Method to handle successful authentication after saml processing.
     * Adds token to security context
     *
     * @param request    request
     * @param response   response
     * @param chain      chain
     * @param authResult authResult
     * @throws IOException      IOException
     * @throws ServletException ServletException
     */

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        Object credentials = authResult.getCredentials();
        if (credentials instanceof SAMLCredential) {
            JWTAuthToken token = SAMLUtils.getJWTAuthToken((SAMLCredential) authResult.getCredentials());
            token.setAuthenticated(true);
            SecurityContextHolder.getContext().setAuthentication(token);
            logger.info("Added JWT to security context for user " + token.getName());
        }
        this.getRememberMeServices().loginSuccess(request, response, authResult);

        if (this.eventPublisher != null) {
            this.eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult,
                    this.getClass()));
        }
        this.getSuccessHandler().onAuthenticationSuccess(request, response, authResult);

    }
}
