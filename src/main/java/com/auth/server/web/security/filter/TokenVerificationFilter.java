package com.auth.server.web.security.filter;

import com.auth.server.AuthServerConstants;
import com.auth.server.exception.InvalidTokenException;
import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.CookieUtils;
import com.auth.server.utils.JWTUtils;
import com.auth.server.utils.RedisHandler;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Entry point for authentication
 * Checks for the presence of jwt in the header or cookie and verifies the token for authenticity.
 */

public class TokenVerificationFilter extends GenericFilterBean {

    protected static final String[] IGNORE_TOKEN_VERIFICATION_PATTERNS =
            new String[]{"/saml/login", "/saml/SSO"};

    private static Logger LOGGER = LoggerFactory.getLogger(TokenVerificationFilter.class);

    /**
     * If the token is absent or invalid, the user is taken through the usual login process.
     * If the token is present, verify the user and redirect him back to the URL from which
     * login was initiated.
     *
     * @param servletRequest  the request
     * @param servletResponse the response
     * @param chain           the filter chain
     * @throws IOException      IOException
     * @throws ServletException ServletException
     */

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        final String token = CookieUtils.extractJWTFromCookieOrHeader(request);
        if (excludeFilter(request) || StringUtils.isEmpty(token)) {
            LOGGER.debug("Bypassing token verification for {}", request.getRequestURI());
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        JWTAuthToken jwtAuthToken = JWTUtils.convertJWTToAuthToken(JWTUtils.verifyJWT(token));
        if (!hasTokenExpired(jwtAuthToken)) {
            jwtAuthToken.setAuthenticated(true);
            SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);
        } else {
            LOGGER.info("Token for {} expired/ not available in redis", jwtAuthToken.getName());
            throw new InvalidTokenException(AuthServerConstants.ERROR_JWT_INVALID);
        }
        LOGGER.info("Verified token for {} in {}", jwtAuthToken.getName(), request.getRequestURI());
        chain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Check if the obtained JWT has expired or is absent in the cache.
     *
     * @param jwtAuthToken Valid JWT from cookie/header
     * @return false if token is alive
     */
    private boolean hasTokenExpired(JWTAuthToken jwtAuthToken) {
        if ((jwtAuthToken.getExpiresOn() - DateTime.now().getMillis() / 1000) > 0
                && RedisHandler.containsEntry(jwtAuthToken.getName(), jwtAuthToken.getTokenId())) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if token verification should be excluded for given request based on
     * a specific pattern (saml entry point and processing endpoint /saml/SSO).
     *
     * @param servletRequest request
     * @return true if exclude false otherwise
     */
    private boolean excludeFilter(HttpServletRequest servletRequest) {
        for (String pattern : IGNORE_TOKEN_VERIFICATION_PATTERNS) {
            if (servletRequest.getRequestURI().contains(pattern)) {
                LOGGER.debug("Ignoring token verification for pattern {}", pattern);
                return true;
            }
        }
        return false;
    }

}
