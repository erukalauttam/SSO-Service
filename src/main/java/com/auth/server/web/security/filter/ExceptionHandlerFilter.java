package com.auth.server.web.security.filter;

import com.auth.server.exception.AuthException;
import com.auth.server.model.Result;
import com.auth.server.utils.BuildResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Filter that catches any exception thrown by the application,
 * allowing a configured handler to deal with it.
 */

public class ExceptionHandlerFilter extends OncePerRequestFilter {

  private static Logger authLogger = LoggerFactory.getLogger(ExceptionHandlerFilter.class);

  @Override
  public void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                               FilterChain filterChain) throws ServletException, IOException {
    try {
      filterChain.doFilter(request, response);

    } catch (AuthException e) {
      setErrorResponse(e.getStatusCode(), response, e);
      authLogger.info("AuthException {}", e.getMessage());
    }
  }

  /**
   * Generate error response when AuthException is thrown.
   * @param status HttpStatus
   * @param response response @{@link HttpServletResponse}
   * @param ex throwable ex
   */

  public void setErrorResponse(HttpStatus status, HttpServletResponse response, Throwable ex) {
    response.setStatus(status != null ? status.value() : HttpStatus.UNAUTHORIZED.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    try {
      Result result = BuildResponse.buildErrorResponse(ex.getMessage(), ex.getClass().getName());
      JSONObject resultJSON = new JSONObject(result);
      response.getWriter().write(resultJSON.toString());
    } catch (IOException e) {
      authLogger.info("Error while writing error response {}", ex.getMessage());
    }
  }

}
