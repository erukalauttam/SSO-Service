package com.auth.server.web.security.filter;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;

/**
 * Extends SAMLEntryPoint to set the RelayState in
 * WebSSOProfileOptions {@link org.springframework.security.saml.websso.WebSSOProfileOptions}.
 * https://docs.spring.io/spring-security-saml/docs/current/api/org/springframework/security/saml/SAMLEntryPoint.html
 */
public class CustomSAMLEntryPoint extends SAMLEntryPoint {

  private static Logger logger = LoggerFactory.getLogger(CustomSAMLEntryPoint.class);

  /*
   * Entry point
   * @param request the request
   * @param response the response
   * @param e Auth exception
   * @throws IOException ioexception
   * @throws ServletException servletexception
   */
  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
                       AuthenticationException e) throws IOException, ServletException {
    setRelayState(request, response);
    super.commence(request, response, (AuthenticationException) null);

  }

  /**
   * Add current request URI to the relay state to preserve for
   * redirecting after successful authentication.
   * @param request request
   * @param response response
   */
  public void setRelayState(HttpServletRequest request, HttpServletResponse response)  {
    super.setDefaultProfileOptions(defaultOptions);
    defaultOptions.setRelayState(request.getParameter("targetUrl"));
    logger.info("Set relay state for the current request to {}", request.getRequestURI());
  }
}
