package com.auth.server.web.security.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

/**
 * Default logout handler to be included during app logout as
 * cookie clearing takes place only after successful logout.
 */
public class DefaultLogoutHandler implements LogoutHandler {

  private static Logger logger = LoggerFactory.getLogger(DefaultLogoutHandler.class);

  /**
   * Causes a logout to be completed. The method must complete successfully.
   *
   * @param request the HTTP request
   * @param response the HTTP response
   * @param authentication the current principal details
   */
  @Override
  public void logout(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) {
    logger.debug("Empty logout handler");
    return;
  }
}
