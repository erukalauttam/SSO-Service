package com.auth.server.web.security.filter;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.CookieUtils;
import com.auth.server.utils.JWTUtils;
import com.auth.server.utils.RedisHandler;
import com.auth.server.utils.Utils;
import com.auth.server.utils.saml.SAMLUtils;
import com.nimbusds.jose.JWSObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.SAMLRelayStateSuccessHandler;

/**
 * Convert the Authentication token to a signed JWT, set it in HTTPResponse as a cookie
 * Relay state is used to store and retrieve the URL to be redirected.
 * Redirect user to the relayState after successful authentication.
 *
 */
  public class CustomAuthenticationSuccessHandler extends SAMLRelayStateSuccessHandler {

  private static Logger logger = LoggerFactory.getLogger(CustomAuthenticationSuccessHandler.class);

  /**
   * If the authentication object is a instance of {@link SAMLCredential}
   * then generates the JWT token and store it in the redis.
   *
   * @param request the request {@link HttpServletRequest}
   * @param response the response {@link HttpServletResponse}
   * @param authentication the authentication object {@link Authentication}
   * @throws IOException ioexception
   * @throws ServletException servletexception
   */
  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    Object credentials = authentication.getCredentials();
    if (credentials instanceof SAMLCredential) {

      JWTAuthToken account = SAMLUtils
          .buildJWTAuthTokenFromSAMLCredential((SAMLCredential) authentication
          .getCredentials());
      JWSObject jwsObject = JWTUtils
          .convertAccountToJWT(account);
      long expiration = Utils.getExpiration(account.getExpiresOn());
      logger.info("Built account object from SAML credential, converted to JWT and "
          + "obtained expiration for user:{}",account.getName());

      RedisHandler.setValue(account.getName(), account.getTokenId(), expiration);

      CookieUtils.addJWTToCookie(jwsObject, response, expiration);
      logger.info("Added JWT to redis cache and response cookie for: {} ",account.getName());
      super.onAuthenticationSuccess(request, response, authentication);
    }
  }
}


