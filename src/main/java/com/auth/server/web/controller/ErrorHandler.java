package com.auth.server.web.controller;

import com.auth.server.exception.AuthException;
import com.auth.server.exception.InvalidTokenException;
import com.auth.server.model.Result;
import com.auth.server.utils.BuildResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Error handler class.
 */

@ControllerAdvice
public class ErrorHandler {

    private static Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

    /**
     * Error Handler for AuthException.
     *
     * @param ex AuthException
     * @return Error response
     */
    @ExceptionHandler({AuthException.class})
    public ResponseEntity<Result> handleAuthException(AuthException ex) {
        logger.info("Authentication exception in Auth Server :{} cause : ",
                ex.getMessage(), ex);
        return new ResponseEntity<>(BuildResponse.buildErrorResponse(ex.getMessage(),
                ex.getClass().getName()), HttpStatus.UNAUTHORIZED);
    }

    /**
     * Error handler for Generic Exceptions.
     *
     * @param ex Exception
     * @return Error response
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Result> handleGenericException(Exception ex) {
        logger.error("Exception in Auth Server :{} cause : ",
                ex.getMessage(), ex);
        return new ResponseEntity<>(BuildResponse.buildErrorResponse(ex.getMessage(),
                ex.getClass().getName()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Error handler for InvalidToken Exceptions.
     *
     * @param ex Exception
     * @return Error response
     */
    @ExceptionHandler({InvalidTokenException.class})
    public ResponseEntity<Result> handleInvalidTokenException(Exception ex) {
        logger.info("InvalidTokenException in Auth Server :{} cause : ",
                ex.getMessage(), ex);
        return new ResponseEntity<>(BuildResponse.buildErrorResponse(ex.getMessage(),
                ex.getClass().getName()), HttpStatus.UNAUTHORIZED);
    }

}
