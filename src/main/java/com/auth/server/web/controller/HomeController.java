package com.auth.server.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.annotations.ApiIgnore;

/**
 * Home redirection to swagger api documentation.
 */
@Controller
@ApiIgnore
public class HomeController {

  /**
   * Redirect / to swagger documentation.
   *
   * @return the resource to redirect to
   */
  @RequestMapping(value = "/home", method = RequestMethod.GET)
  public String index() {
    return "redirect:swagger-ui.html";
  }
}
