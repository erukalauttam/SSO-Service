package com.auth.server.web.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller for logout flow through browser.
 */
@Controller
@RequestMapping("/web")
public class WebController {

    @Value("${onelogin.appId}")
    @Getter
    @Setter
    private String appId;

    @Value("${onelogin.entityId}")
    @Getter
    @Setter
    private String entityId;


    @GetMapping(value = "/logout")
    public void logout(HttpServletResponse response)
            throws IOException {
        response.sendRedirect("/saml/logout?appId=" + appId
                + "&entityId=" + entityId);
    }

    @GetMapping(value = "/login")
    public void login(HttpServletResponse response)
            throws IOException {
        response.sendRedirect("/resource/hello");
    }
}
