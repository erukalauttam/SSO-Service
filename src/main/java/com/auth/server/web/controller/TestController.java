package com.auth.server.web.controller;

import com.auth.server.model.Result;
import com.auth.server.utils.BuildResponse;
import com.auth.server.utils.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * REST APIs for Authentication test.
 */
@RestController
@RequestMapping("/resource")
@Api(value = "Sample REST API with Authentication")
public class TestController {

    /**
     * This endpoint is a sample API which requires JWT in auth_session to allow access.
     */
    @ApiOperation(value = "Example REST API authentication", notes = "This endpoint is a "
            + "sample API which requires JWT in auth_session header"
            + "to allow access.", response = String.class, tags = {
            "Hello from Rest API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Result.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = Result.class),
            @ApiResponse(code = 403, message = "Forbidden", response = Result.class)})

    @GetMapping(value = "/hello")
    public Result sayHello() {
        Optional<Object> principal = Utils.resolve(() -> SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal());
        return BuildResponse.buildSuccessResponse(principal.isPresent() ? principal.get()
                .toString() : "anonymous");
    }

}
