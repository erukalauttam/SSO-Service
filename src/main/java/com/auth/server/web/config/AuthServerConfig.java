package com.auth.server.web.config;

import com.auth.server.AuthServerConstants;
import com.auth.server.exception.DataStoreUnavailableException;
import com.auth.server.utils.RedisHandler;
import com.auth.server.web.security.filter.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.saml2.metadata.provider.HTTPMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLBootstrap;
import org.springframework.security.saml.context.SAMLContextProviderImpl;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.key.KeyManager;
import org.springframework.security.saml.log.SAMLDefaultLogger;
import org.springframework.security.saml.metadata.*;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.security.saml.processor.HTTPPostBinding;
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding;
import org.springframework.security.saml.processor.SAMLBinding;
import org.springframework.security.saml.processor.SAMLProcessorImpl;
import org.springframework.security.saml.storage.EmptyStorageFactory;
import org.springframework.security.saml.util.VelocityFactory;
import org.springframework.security.saml.websso.*;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.NullSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.*;

/**
 * Auth server configuration.
 */
@Configuration
@EnableWebSecurity
public class AuthServerConfig extends WebSecurityConfigurerAdapter {

    private static Logger logger = LoggerFactory.getLogger(AuthServerConfig.class);

    @Value("${app.redis.endpoint}")
    private String redisEndpoint;

    @Value("${server.ssl.key-store}")
    @Getter
    @Setter
    private Resource keyStore;

    @Value("${server.ssl.key-store-password}")
    @Getter
    @Setter
    private String secret;

    @Value("${server.ssl.key-alias}")
    @Getter
    @Setter
    private String alias;

    @Value("${onelogin.entityId}")
    @Getter
    @Setter
    private String entityId;

    @Value("${onelogin.appId}")
    @Getter
    @Setter
    private String appId;

    @Value("${onelogin.postLogout:#{'/'}}")
    @Getter
    @Setter
    private String postLogoutURL;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.addFilterAt((tokenVerificationFilter()), BasicAuthenticationFilter.class)
                .addFilterBefore(exceptionHandlerFilter(), TokenVerificationFilter.class);

        http.exceptionHandling()
                .defaultAuthenticationEntryPointFor(
                        samlEntryPoint(),
                        new AntPathRequestMatcher("/**"));

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(samlFilter(), TokenVerificationFilter.class)
                .addFilterBefore(metadataGeneratorFilter(), FilterChainProxy.class);
        http
                .authorizeRequests()
                .antMatchers("/api/v1/login").permitAll()
                .antMatchers("/swagger-ui.html/**").permitAll()
                .antMatchers("/v2/api-docs/**").permitAll()
                .antMatchers("/health/**").permitAll()
                .antMatchers("/info/**").permitAll()
                .anyRequest().authenticated();
        http.requiresChannel().anyRequest();
        http
                .logout()
                .logoutSuccessUrl("/");
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/favicon.ico/**", "/webjars/**", "/swagger-resources/**");
    }


    /**
     * Creates and initialize redis handler with redis endpoint url
     * if not available connects through AWS Elastic cache auto discovery
     * with env variables AWS_ACCESS_KEY_ID ,AWS_SECRET_ACCESS_KEY and AWS_REGION.
     *
     * @return redisHandler
     */
    @Bean
    public RedisHandler redisHandler() {
        RedisHandler redisHandler = new RedisHandler();
        try {
            redisHandler.initialize(redisEndpoint);
        } catch (DataStoreUnavailableException ex) {
            logger.warn("Error occured in connecting through redis url:{}", redisEndpoint);
        }
        return redisHandler;
    }

    /**
     * Token verification filter.
     *
     * @return @{@link TokenVerificationFilter}
     */
    @Bean
    public TokenVerificationFilter tokenVerificationFilter() {
        return new TokenVerificationFilter();
    }

    /**
     * Exception handler filter.
     *
     * @return @{@link ExceptionHandlerFilter}
     */
    @Bean
    public ExceptionHandlerFilter exceptionHandlerFilter() {
        return new ExceptionHandlerFilter();
    }

    /**
     * SAML library initialization.
     *
     * @return sAMLBootstrap the samlbootstrap
     */
    @Bean
    public static SAMLBootstrap samlBootstrap() {
        return new SAMLBootstrap();
    }


    /**
     * Authentication Manager builder.
     *
     * @param auth the {@link AuthenticationManagerBuilder} object
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth
                .authenticationProvider(samlAuthenticationProvider());
    }

    /**
     * SAML entry point.
     *
     * @return CustomSAMLEntryPoint
     */
    @Bean
    public CustomSAMLEntryPoint samlEntryPoint() {
        CustomSAMLEntryPoint samlEntryPoint = new CustomSAMLEntryPoint();
        samlEntryPoint.setDefaultProfileOptions(defaultWebSsoProfileOptions());
        return samlEntryPoint;
    }


    /**
     * Overriden to convert the Authentication token to a signed JWT, set it in
     * HTTPResponse as a cookie
     * Relay state is used to store and retrieve the URL to be redirected.
     * Redirect user to the relayState after successful authentication.
     *
     * @return CustomAuthenticationSuccessHandler
     */
    @Bean
    public CustomAuthenticationSuccessHandler successRedirectHandler() {
        CustomAuthenticationSuccessHandler successRedirectHandler =
                new CustomAuthenticationSuccessHandler();
        successRedirectHandler.setDefaultTargetUrl("/");
        return successRedirectHandler;
    }

    /**
     * creates a samlProcessingFilter for singleSignOn
     * configured with success and failure handlers.
     *
     * @return CustomSAMLProcessingFilter {@link CustomSAMLProcessingFilter} .
     * @throws Exception occurs when error in initializing the {@link AuthenticationManager}.
     */
    @Bean
    public CustomSAMLProcessingFilter samlWebSsoProcessingFilter() throws Exception {
        CustomSAMLProcessingFilter samlWebSsoProcessingFilter = new CustomSAMLProcessingFilter();
        samlWebSsoProcessingFilter.setAuthenticationManager(authenticationManager());
        samlWebSsoProcessingFilter.setAuthenticationSuccessHandler(successRedirectHandler());
        samlWebSsoProcessingFilter.setAuthenticationFailureHandler(authenticationFailureHandler());
        return samlWebSsoProcessingFilter;
    }

    /**
     * Filterchain flow of SAML filter.
     *
     * @return FilterChainProxy the FilterChainProxy
     * @throws Exception thrown when error in configuring  the {@link CustomSAMLProcessingFilter}.
     */

    @Bean
    public FilterChainProxy samlFilter() throws Exception {
        List<SecurityFilterChain> chains = new ArrayList<>();

        chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/metadata/**"),
                metadataDisplayFilter()));

        chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/login/**"),
                samlEntryPoint()));

        chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SSO/**"),
                samlWebSsoProcessingFilter()));

        chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/logout/**"),
                samlLogoutFilter()));

        chains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher("/saml/SingleLogout/**"),
                samlLogoutProcessingFilter()));

        return new FilterChainProxy(chains);
    }

    /**
     * Set default profile options for saml SSO.
     *
     * @return profile options
     */
    @Bean
    public WebSSOProfileOptions defaultWebSsoProfileOptions() {
        WebSSOProfileOptions webSsoProfileOptions = new WebSSOProfileOptions();
        webSsoProfileOptions.setIncludeScoping(false);
        return webSsoProfileOptions;
    }

    /**
     * Instantiate {@link MetadataDisplayFilter}}.
     *
     * @return MetadataDisplayFilter
     */
    @Bean
    public MetadataDisplayFilter metadataDisplayFilter() {
        return new MetadataDisplayFilter();
    }

    /**
     * Instantiate {@link SimpleUrlAuthenticationFailureHandler}.
     *
     * @return SimpleUrlAuthenticationFailureHandler
     */
    @Bean
    public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler() {
        return new SimpleUrlAuthenticationFailureHandler();
    }

    /**
     * HttpStatusReturningLogoutSuccessHandler.
     *
     * @return HttpStatusReturningLogoutSuccessHandler
     */
    @Bean
    public SimpleUrlLogoutSuccessHandler successLogoutHandler() {
        SimpleUrlLogoutSuccessHandler simpleUrlLogoutSuccessHandler =
                new SimpleUrlLogoutSuccessHandler();
        simpleUrlLogoutSuccessHandler.setDefaultTargetUrl(postLogoutURL);
        simpleUrlLogoutSuccessHandler.setAlwaysUseDefaultTargetUrl(true);
        return simpleUrlLogoutSuccessHandler;
    }

    /**
     * Instantiate {@link LogoutHandler}.
     *
     * @return LogoutHandler
     */
    @Bean
    public LogoutHandler[] logoutHandlers() {
        return new LogoutHandler[]{logoutHandler()};
    }

    /**
     * Instantiate {@link DefaultLogoutHandler}.
     *
     * @return DefaultLogoutHandler
     */
    public DefaultLogoutHandler logoutHandler() {
        return new DefaultLogoutHandler();
    }

    /**
     * Instantiate {@link CustomSAMLLogoutFilter}.
     *
     * @return SAMLLogoutFilter
     */
    @Bean
    public CustomSAMLLogoutFilter samlLogoutFilter() {
        return new CustomSAMLLogoutFilter(successLogoutHandler(), logoutHandlers(), logoutHandlers());
    }

    /**
     * SAMLLogoutProcessingFilter.
     *
     * @return SAMLLogoutProcessingFilter
     */
    @Bean
    public CustomSAMLLogoutProcessingFilter samlLogoutProcessingFilter() {
        CookieClearingLogoutHandler cookieClearingLogoutHandler = new CookieClearingLogoutHandler(
                new String[]{AuthServerConstants.ONELOGIN_COOKIE_NAME});
        SecurityContextLogoutHandler securityContextLogoutHandler =
                new SecurityContextLogoutHandler();
        securityContextLogoutHandler.setInvalidateHttpSession(true);
        securityContextLogoutHandler.setClearAuthentication(true);
        return new CustomSAMLLogoutProcessingFilter(successLogoutHandler(),
                new LogoutHandler[]{cookieClearingLogoutHandler, securityContextLogoutHandler});
    }

    /**
     * Instantiate {@link SecurityContextRepository}.
     *
     * @return NullSecurityContextRepository.
     */
    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new NullSecurityContextRepository();
    }

    /**
     * Instantiate {@link SecurityContextPersistenceFilter}.
     *
     * @return securityContextPersistenceFilter
     */
    @Bean
    public SecurityContextPersistenceFilter securityContextPersistenceFilter() {
        return new SecurityContextPersistenceFilter(securityContextRepository());
    }


    /**
     * Instantiate {@link MetadataGeneratorFilter}
     * with {@link MetadataGenerator} configuration properties.
     *
     * @return MetadataGeneratorFilter
     */
    @Bean
    public MetadataGeneratorFilter metadataGeneratorFilter() {
        return new MetadataGeneratorFilter(metadataGenerator());
    }

    /**
     * For configuring the metaData properties of {@link MetadataGenerator}.
     *
     * @return MetadataGenerator
     */
    @Bean
    public MetadataGenerator metadataGenerator() {
        MetadataGenerator metadataGenerator = new MetadataGenerator();
        metadataGenerator.setEntityId(getEntityId());
        metadataGenerator.setExtendedMetadata(extendedMetadata());
        metadataGenerator.setIncludeDiscoveryExtension(false);
        metadataGenerator.setKeyManager(keyManager());
        return metadataGenerator;
    }

    /**
     * For configuring the {@link ExtendedMetadata} properties.
     *
     * @return ExtendedMetadata
     */
    @Bean
    public ExtendedMetadata extendedMetadata() {
        ExtendedMetadata extendedMetadata = new ExtendedMetadata();
        extendedMetadata.setIdpDiscoveryEnabled(false);
        extendedMetadata.setSignMetadata(false);
        return extendedMetadata;
    }


    /**
     * Instantiate {@link VelocityEngine}.
     *
     * @return VelocityEngine
     */
    @Bean
    public VelocityEngine velocityEngine() {
        return VelocityFactory.getEngine();
    }

    /**
     * Instantiate {@link StaticBasicParserPool}.
     *
     * @return StaticBasicParserPool
     */
    @Bean(initMethod = "initialize")
    public StaticBasicParserPool parserPool() {
        return new StaticBasicParserPool();
    }

    /**
     * Instantiate {@link ParserPoolHolder}.
     *
     * @return ParserPoolHolder
     */
    @Bean(name = "parserPoolHolder")
    public ParserPoolHolder parserPoolHolder() {
        return new ParserPoolHolder();
    }

    /**
     * Instantiate {@link HTTPPostBinding}.
     *
     * @return HTTPPostBinding
     */
    @Bean
    public HTTPPostBinding httpPostBinding() {
        return new HTTPPostBinding(parserPool(), velocityEngine());
    }

    /**
     * Instantiate {@link HTTPRedirectDeflateBinding}.
     *
     * @return HTTPRedirectDeflateBinding
     */
    @Bean
    public HTTPRedirectDeflateBinding httpRedirectDeflateBinding() {
        return new HTTPRedirectDeflateBinding(parserPool());
    }

    /**
     * Instantiate {@link SAMLProcessorImpl}
     * with {@link HTTPRedirectDeflateBinding} and {@link HTTPPostBinding}.
     *
     * @return SAMLProcessorImpl
     */
    @Bean
    public SAMLProcessorImpl processor() {
        Collection<SAMLBinding> bindings = new ArrayList<>();
        bindings.add(httpRedirectDeflateBinding());
        bindings.add(httpPostBinding());
        return new SAMLProcessorImpl(bindings);
    }


    /**
     * Instantiate {@link SAMLDefaultLogger}.
     *
     * @return SAMLDefaultLogger the logger
     */
    @Bean
    public SAMLDefaultLogger samlLogger() {
        return new SAMLDefaultLogger();
    }

    /**
     * contextProvider.
     *
     * @return SAMLContextProviderImpl the context provider
     */
    @Bean
    public SAMLContextProviderImpl contextProvider() {
        SAMLContextProviderImpl samlContextProvider = new SAMLContextProviderImpl();
        samlContextProvider.setStorageFactory(new EmptyStorageFactory());
        return samlContextProvider;
    }

    // SAML 2.0 WebSSO Assertion Consumer
    @Bean
    public WebSSOProfileConsumer webSSOprofileConsumer() {
        return new WebSSOProfileConsumerImpl();
    }

    // SAML 2.0 Web SSO profile
    @Bean
    public WebSSOProfile webSSOprofile() {
        return new WebSSOProfileImpl();
    }

    // not used but autowired...
    // SAML 2.0 Holder-of-Key WebSSO Assertion Consumer
    @Bean
    public WebSSOProfileConsumerHoKImpl hokWebSSOprofileConsumer() {
        return new WebSSOProfileConsumerHoKImpl();
    }

    // not used but autowired...
    // SAML 2.0 Holder-of-Key Web SSO profile
    @Bean
    public WebSSOProfileConsumerHoKImpl hokWebSSOProfile() {
        return new WebSSOProfileConsumerHoKImpl();
    }

    /**
     * Instantiate {@link SingleLogoutProfile}.
     *
     * @return SingleLogoutProfile logout profile
     */
    @Bean
    public SingleLogoutProfile logoutprofile() {
        return new SingleLogoutProfileImpl();
    }

    /**
     * idpMetadata for making saml assertion request.
     *
     * @return ExtendedMetadataDelegate metadata delegate
     * @throws MetadataProviderException if error in initializing
     *                                   the {@link HTTPMetadataProvider}.
     */
    @Bean
    public ExtendedMetadataDelegate idpMetadata()
            throws MetadataProviderException {
        Timer backgroundTaskTimer = new Timer(true);
        HTTPMetadataProvider httpMetadataProvider = new HTTPMetadataProvider(backgroundTaskTimer,
                new HttpClient(), AuthServerConstants.ONELOGIN_METADATA_URL.concat(getAppId()));
        httpMetadataProvider.setParserPool(parserPool());

        ExtendedMetadataDelegate extendedMetadataDelegate =
                new ExtendedMetadataDelegate(httpMetadataProvider, extendedMetadata());
        extendedMetadataDelegate.setMetadataTrustCheck(true);
        extendedMetadataDelegate.setMetadataRequireSignature(false);
        return extendedMetadataDelegate;
    }

    /**
     * metadata.
     *
     * @return CachingMetadataManager caching metadata manager
     * @throws MetadataProviderException if error in initializing
     *                                   the {@link CachingMetadataManager}.
     */
    @Bean
    @Qualifier("metadata")
    public CachingMetadataManager metadata() throws MetadataProviderException {
        List<MetadataProvider> providers = new ArrayList<>();
        providers.add(idpMetadata());
        return new CachingMetadataManager(providers);
    }

    /**
     * Instantiate {@link SAMLAuthenticationProvider}.
     *
     * @return SAMLAuthenticationProvider the authentication provider
     */
    @Bean
    public SAMLAuthenticationProvider samlAuthenticationProvider() {
        SAMLAuthenticationProvider samlAuthenticationProvider = new SAMLAuthenticationProvider();
        samlAuthenticationProvider.setForcePrincipalAsString(false);
        return samlAuthenticationProvider;
    }


    /**
     * Keystore manager for initializing the {@link KeyManager} with all the credentials.
     *
     * @return {@link JKSKeyManager}.
     */

    @Bean
    public KeyManager keyManager() {
        String storePass = getSecret();
        Map<String, String> passwords = new HashMap<>();
        passwords.put(alias, getSecret());
        return new JKSKeyManager(keyStore, storePass, passwords, alias);
    }

}

