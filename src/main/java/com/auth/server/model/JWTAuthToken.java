package com.auth.server.model;

import com.auth.server.AuthServerConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomUtils;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.*;


/**
 * JWTAuthToken - authentication object configured with
 * regarding authentication token, user details and credentials.
 */
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class JWTAuthToken implements Authentication {

  @JsonIgnore
  private final transient Object principal;
  @JsonIgnore
  private transient Object credentials;

  private Collection<? extends GrantedAuthority> authorities;
  @JsonIgnore
  private transient Object details;
  @JsonIgnore
  private boolean authenticated = false;

  @Getter
  @Setter
  private String tokenId;

  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private String firstName;

  @Getter
  @Setter
  private String lastName;

  @Getter
  @Setter
  private String email;

  @Getter
  @Setter
  private Map<String, List<String>> attributes = new HashMap<>();

  @Getter
  @Setter
  private Long expiresOn;

  @Getter
  @Setter
  private String organization;

  @Getter
  @Setter
  private String sessionIndex;

  /**
   * Create usertoken with principal and credentials.
   *
   * @param principal   the entity that currently logged in .
   * @param credentials the credentials of the logged in entity.
   */
  public JWTAuthToken(Object principal, Object credentials) {
    this.tokenId = RandomUtils.nextLong() + "";
    this.principal = principal;
    this.credentials = credentials;
  }

  /**
   * Create user token with principal, credentials and authorities.
   *
   * @param principal   the entity that currently logged in .
   * @param credentials the credentials of the logged in entity.
   * @param authorities the authorities of the logged in entity.
   */
  public JWTAuthToken(Object principal, Object credentials, Collection<? extends GrantedAuthority>
      authorities) {
    this.tokenId = RandomUtils.nextLong() + "";
    this.principal = principal;
    this.credentials = credentials;
    this.setAuthorities(authorities);
  }


  @Override
  public Object getCredentials() {
    return credentials;
  }

  @Override
  public Object getPrincipal() {
    return principal;
  }

  /**
   * Given a JSON containing user information, populate JWTAuthToken fields
   * with these values.
   *
   * @param tokenJSON token with details of the logged in entity.
   */
  public void populateAccount(JSONObject tokenJSON) {
    this.tokenId = tokenJSON.getString(AuthServerConstants.TOKEN_ID);
    this.name = tokenJSON.getString(AuthServerConstants.NAME);
    this.email = tokenJSON.getString(AuthServerConstants.EMAIL);
    this.organization = tokenJSON.has(AuthServerConstants.COMPANY) ? tokenJSON
        .getString(AuthServerConstants.COMPANY) : null;
    this.sessionIndex = tokenJSON.has(AuthServerConstants.SESSION_INDEX) ? tokenJSON
        .getString(AuthServerConstants.SESSION_INDEX) : null;
    this.expiresOn = tokenJSON.getLong(AuthServerConstants.EXPIRATION);
  }

  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.authorities;
  }

  /**
   * Set authorities.
   *
   * @param authorities granted authorities
   */
  public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
    if (authorities == null) {
      this.authorities = AuthorityUtils.NO_AUTHORITIES;
      return;
    }

    for (GrantedAuthority a : authorities) {
      if (a == null) {
        throw new IllegalArgumentException(
            "Authorities collection cannot contain any null elements");
      }
    }
    ArrayList<GrantedAuthority> temp = new ArrayList<>(
        authorities.size());
    temp.addAll(authorities);
    this.authorities = Collections.unmodifiableList(temp);
  }

  public boolean isAuthenticated() {
    return authenticated;
  }

  public void setAuthenticated(boolean authenticated) {
    this.authenticated = authenticated;
  }

  public Object getDetails() {
    return details;
  }

  public void setDetails(Object details) {
    this.details = details;
  }

  @Override
  public String toString() {
    StringBuilder authTokenString = new StringBuilder();
    authTokenString.append(super.toString()).append(": ");
    authTokenString.append("Principal: ").append(this.getPrincipal()).append("; ").append("Credentials: [PROTECTED]; ");
    authTokenString.append("Authenticated: ").append(this.isAuthenticated()).append("; ");
    authTokenString.append("Details: ").append(this.getDetails()).append("; ");
    if (!authorities.isEmpty()) {
      authTokenString.append("Granted Authorities: ");
      int i = 0;
      for (GrantedAuthority authority : authorities) {
        if (i++ > 0) {
          authTokenString.append(", ");
        }
        authTokenString.append(authority);
      }
    } else {
      authTokenString.append("Not granted any authorities");
    }
    return authTokenString.toString();
  }
}
