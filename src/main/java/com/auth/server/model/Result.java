package com.auth.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Response object for Controllers that holds the status, data
 * and error message/error if any.
 */
@ToString
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result {

  @Getter @Setter
  private String status;

  @Getter @Setter
  private String errorMessage;

  @Getter @Setter
  private Object data;

  @Getter @Setter
  private String error;

  /**
   * Constructor.
   */

  public Result() {

  }

  /**
   * Constructor to set the status, message and data.
   * @param status the status of the result i.e,failure or Error.
   * @param errorMessage  the errorMessage to be shown .
   * @param error Type of the error.
   */
  public Result(String status, String errorMessage,  String error) {
    this.status = status;
    this.errorMessage = errorMessage;
    this.error = error;
  }

  /**
   * Constructor to set the message and data.
   * @param status the status the status of the result i.e, Success,failure or Error.
   * @param data the response of the Result object.
   */
  public Result(String status, Object data) {
    this.status = status;
    this.data = data;
  }

}
