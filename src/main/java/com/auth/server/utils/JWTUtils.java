package com.auth.server.utils;

import com.auth.server.AuthServerConstants;
import com.auth.server.exception.AuthException;
import com.auth.server.exception.InvalidTokenException;
import com.auth.server.model.JWTAuthToken;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.json.JSONObject;
import org.springframework.security.core.GrantedAuthority;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * Utility methods for JWT to JWTAuthToken @{@link JWTAuthToken}
 * conversion , vice versa , JWT verification and generation.
 */
public class JWTUtils {

  private JWTUtils() {

  }

  /**
   * Verify and extract user from the JWT.
   *
   * @param token the token
   * @return user the user @{@link JWTAuthToken}
   */
  public static String verifyJWT(String token) {
    try {
      JWSObject jwsObject = JWSObject.parse(token);
      JWSVerifier verifier = new MACVerifier(AuthServerConstants.JWT_SECRET);
      if (jwsObject.verify(verifier) == Boolean.TRUE) {
        return jwsObject.getPayload().toString();
      } else {
        throw new InvalidTokenException(AuthServerConstants.ERROR_JWT_INVALID);
      }
    } catch (ParseException | JOSEException ex) {
      throw new InvalidTokenException(AuthServerConstants.ERROR_JWT_INVALID, ex);
    }
  }

  /**
   * Converts JWT token to Auth token.
   *
   * @param accountJSON accountJSON
   * @return jwtAuthToken @{@link JWTAuthToken}
   */
  public static JWTAuthToken convertJWTToAuthToken(String accountJSON) {
    JWTAuthToken jwtAuthToken;
    JSONObject tokenJSON = new JSONObject(accountJSON);
    jwtAuthToken = new JWTAuthToken(tokenJSON.get(AuthServerConstants.NAME), tokenJSON.get(AuthServerConstants.EMAIL),
        Utils.getSimpleGrantedAuthorities(tokenJSON.getJSONArray(AuthServerConstants.AUTHORITIES).toList()));

    jwtAuthToken.populateAccount(tokenJSON);
    return jwtAuthToken;
  }

  /**
   * Convert the JWTAuthToken object to a JWT.
   *
   * @param account the jwtAuthToken @{@link JWTAuthToken} object.
   * @return JWT the token.
   */
  public static SignedJWT convertAccountToJWT(JWTAuthToken account) {
    try {
      JWTClaimsSet jwtClaims = getClaimsSet(account);
      return getSignedJWT(jwtClaims);
    } catch (JOSEException e) {
      throw new InvalidTokenException(AuthServerConstants.ERROR_JWT_INVALID, e.getMessage());
    }
  }


  /**
   * Get List of roles as String list from list of GrantedAuthority.
   *
   * @param authorities the authority list
   * @return roles list @{@link org.springframework.security.core.authority.SimpleGrantedAuthority}.
   */
  private static List<String> getRoles(Collection<? extends GrantedAuthority> authorities) {
    List<String> roles = new ArrayList<>();
    for (GrantedAuthority authority : authorities) {
      roles.add(authority.getAuthority());
    }
    return roles;
  }

  /**
   * Create JWTClaimsSet from JWTAuthToken.
   *
   * @param account jwtAuthToken
   * @return JWTClaimsSet
   */
  private static JWTClaimsSet getClaimsSet(JWTAuthToken account) {
    return new JWTClaimsSet.Builder().subject(account.getName())
        .expirationTime(new Date(account.getExpiresOn() * 1000))
        .issueTime(new Date()).issuer(AuthServerConstants.AUTH)
        .claim(AuthServerConstants.TOKEN_ID, account.getTokenId()).claim(AuthServerConstants.EMAIL, account.getEmail())
        .claim(AuthServerConstants.NAME, account.getName()).claim(AuthServerConstants.COMPANY, account.getOrganization())
        .claim(AuthServerConstants.SESSION_INDEX, account.getSessionIndex())
        .claim(AuthServerConstants.FIRSTNAME, account.getFirstName())
        .claim(AuthServerConstants.LASTNAME, account.getLastName()).claim(AuthServerConstants.ATTRIBUTES, account.getAttributes())
        .claim(AuthServerConstants.AUTHORITIES, getRoles(account.getAuthorities())).build();
  }

  /**
   * Sign jwtClaims with JWT_SECRET.
   *
   * @param jwtClaims the jwtClaims
   * @return Signed JWT
   * @throws JOSEException JOSEException thrown when there are issues with signing the message
   */
  private static SignedJWT getSignedJWT(JWTClaimsSet jwtClaims) throws JOSEException {
    SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), jwtClaims);
    JWSSigner signer = new MACSigner(AuthServerConstants.JWT_SECRET);
    signedJWT.sign(signer);
    return signedJWT;
  }

}
