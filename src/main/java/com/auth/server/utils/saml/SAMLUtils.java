package com.auth.server.utils.saml;

import javax.servlet.http.HttpServletRequest;

import com.auth.server.AuthServerConstants;
import com.auth.server.model.JWTAuthToken;
import com.auth.server.utils.CookieUtils;
import com.auth.server.utils.JWTUtils;
import com.auth.server.utils.Utils;
import org.apache.commons.lang.StringUtils;
import org.opensaml.Configuration;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.NameID;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.springframework.security.saml.SAMLCredential;

/**
 * Utils for SAML.
 */
public class SAMLUtils {


  /**
   * Given an Authentication Object, convert it into @{@link JWTAuthToken}.
   *
   * @param credential the auth object
   * @return user @{@link JWTAuthToken}
   */
  public static JWTAuthToken buildJWTAuthTokenFromSAMLCredential(SAMLCredential credential) {
    return Utils.buildAttributes(credential.getNameID().getValue(),
        credential.getAttributes(),
        Utils.getExpiration(credential.getAuthenticationAssertion()
            .getAuthnStatements()), Utils.getSessionIndex(credential
            .getAuthenticationAssertion()
            .getAuthnStatements()));
  }

  /**
   * add SAMLCredentials to JWTAuthToken.
   *
   * @param credential the credential
   * @return JWTAuthToken
   */
  public static JWTAuthToken buildJWTAuthTokenWithSAMLCredential(SAMLCredential credential) {
    return Utils.buildAttributes(credential.getNameID().getValue(), credential,
        credential.getAttributes(),
        Utils.getExpiration(credential.getAuthenticationAssertion()
            .getAuthnStatements()));
  }

  /**
   * Get a JWTAuthToken from authentication object.
   *
   * @param credential the auth object.
   * @return the user token.
   */
  public static JWTAuthToken getJWTAuthToken(SAMLCredential credential) {
    return buildJWTAuthTokenFromSAMLCredential(credential);
  }

  /**
   * Check for JWT in cookie, verify JWT and covert to JWTAuthToken.
   *
   * @param req request
   * @return JWTAuthToken
   */
  public static JWTAuthToken getAuthTokenFromRequest(HttpServletRequest req) {

    final String token = CookieUtils
        .extractJWTFromCookieOrHeader(req);
    if (StringUtils.isEmpty(token)) {
      return null;
    }
    String payload = JWTUtils.verifyJWT(token);
    JWTAuthToken jwtAuthToken = JWTUtils.convertJWTToAuthToken(payload);
    return jwtAuthToken;
  }

  /**
   * Generate SAMLCredential with name, sessionId, appId and localEntityId.
   *
   * @param name         name attribute in {@link JWTAuthToken} .
   * @param sessionIndex sessionIndex attribute in {@link JWTAuthToken}.
   * @param appId        SAML Onelogin appId.
   * @param entityId     SAML Onelogin entityId.
   * @return {@link SAMLCredential}
   */
  public static SAMLCredential generateSAMLCredential(String name, String sessionIndex,
                                                      String appId, String entityId) {

    XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
    NameID nameID = ((SAMLObjectBuilder<NameID>) builderFactory
        .getBuilder(NameID.DEFAULT_ELEMENT_NAME)).buildObject();
    nameID.setValue(name);
    nameID.setFormat(AuthServerConstants.NAMEID_FORMAT);
    Assertion assertion = ((SAMLObjectBuilder<Assertion>) builderFactory
        .getBuilder(Assertion.DEFAULT_ELEMENT_NAME)).buildObject();
    assertion.getAuthnStatements().add(generateAuthStatement(builderFactory, sessionIndex));
    SAMLCredential credential = new SAMLCredential(nameID, assertion,
        AuthServerConstants.ONELOGIN_METADATA_URL + appId, entityId);

    return credential;
  }

  /**
   * generate an AuthStatement with the sessionIndex.
   *
   * @param builderFactory Instance of {@link SAMLObjectBuilder}.
   * @param sessionIndex   sessionIndex identifies which session to terminate/logout.
   * @return {@link AuthnStatement}
   */
  public static AuthnStatement generateAuthStatement(XMLObjectBuilderFactory builderFactory,
                                                     String sessionIndex) {

    AuthnStatement statement = ((SAMLObjectBuilder<AuthnStatement>) builderFactory
        .getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME)).buildObject();
    statement.setSessionIndex(sessionIndex);

    return statement;
  }
}
