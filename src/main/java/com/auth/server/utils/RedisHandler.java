package com.auth.server.utils;

import com.auth.server.AuthServerConstants;
import com.auth.server.exception.DataStoreUnavailableException;
import org.redisson.Redisson;
import org.redisson.api.RSetCache;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


/**
 * Handler for Redis connection and communication with Redis cache.
 */
public class RedisHandler {

  private static Logger logger = LoggerFactory.getLogger(RedisHandler.class);

  /**
   * Initialize Redis connection.
   */

  public static RedissonClient getCache() {
    return redisson;
  }

  private static RedissonClient redisson;

  private static RSetCache<String> redisSet;


  /**
   * Static block to initialize Redis client and establish connection.
   *
   * @param endpoint redis url Endpoint
   */
  public static void initialize(String endpoint) {
    try {
      Config config = new Config();
      config.useSingleServer().setAddress(endpoint);
      config.setCodec(new StringCodec());
      redisson = Redisson.create(config);
      redisSet = getCache().getSetCache(AuthServerConstants.JWT_SESSION_STORE);
    } catch (Exception ex) {
      logger.error("Error in connecting to redis Endpoint:{} ", endpoint);
      throw new DataStoreUnavailableException(AuthServerConstants.ERROR_REDIS, ex.getMessage());
    }
  }

  /**
   * Stores the key name:tokenId in the redis cache Set with an expiration period.
   *
   * @param name       the name of the JWTAuthToken @{@link JWTAuthToken}.
   * @param tokenId    the tokenId of the JWTAuthToken @{@link JWTAuthToken}.
   * @param expiration expiration period of the value.
   */
  public static void setValue(String name, String tokenId, long expiration) {
    redisSet.add(getKey(name, tokenId), expiration, TimeUnit.SECONDS);
  }

  /**
   * Checks if name:tokenId exists in cache.
   *
   * @param name    the name of the JWTAuthToken @{@link JWTAuthToken}.
   * @param tokenId the tokenId of the JWTAuthToken @{@link JWTAuthToken}.
   * @return true is exists, false otherwise.
   */
  public static boolean containsEntry(String name, String tokenId) {
    return redisSet.contains(getKey(name, tokenId));
  }

  /**
   * delete name:tokenId from Redis set.
   *
   * @param name    the name of the JWTAuthToken @{@link JWTAuthToken}.
   * @param tokenId the tokenId of the JWTAuthToken @{@link JWTAuthToken}.
   * @return true if delete is successful
   */
  public static Object deleteEntry(String name, String tokenId) {
    return redisSet.remove(getKey(name, tokenId));
  }


  /**
   * Flush map contents.
   */
  public static void flushMap() {
    redisSet.delete();
  }

  public static String getKey(String name, String tokenId) {
    return name + AuthServerConstants.CHAR_COLON + tokenId;
  }
}
