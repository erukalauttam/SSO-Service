package com.auth.server.utils;


import com.auth.server.AuthServerConstants;
import com.auth.server.model.Result;

/**
 * Builds success or error response as @{@link Result} for
 * APIs with appropriate data/message.
 */
public class BuildResponse {

  private BuildResponse() { }

  /**
   * Build error response.
   * @param message reason for the response error.
   * @param error Type of the error .
   * @return the Result @{@link Result} Object
   */
  public static Result buildErrorResponse(String message, String error) {
    return new Result(AuthServerConstants.ERROR, message, error);
  }

  /**
   * Builds success response.
   * @param data success response body.
   * @return the Result @{@link Result} Object
   */
  public static Result buildSuccessResponse(Object data) {
    return new Result(AuthServerConstants.SUCCESS, data);
  }

  /**
   * Builds success response.
   * @return the Result @{@link Result} Object
   */
  public static Result buildSuccessResponse() {
    return new Result(AuthServerConstants.SUCCESS, null);
  }
}
