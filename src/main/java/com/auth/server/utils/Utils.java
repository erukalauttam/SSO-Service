package com.auth.server.utils;

import com.auth.server.AuthServerConstants;
import com.auth.server.model.JWTAuthToken;
import com.auth.server.model.Result;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.impl.AttributeImpl;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.impl.XSStringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;

/**
 * Utility methods for authentication, building attributes from assertion, exposing loggedIn user details.
 */

public class Utils {

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    private static final String FIRSTNAME = "User.FirstName";
    private static final String LASTNAME = "User.LastName";
    private static final String EMAIL = "User.email";
    private static final String MEMBEROF = "memberOf";
    private static final String COMPANY = "Company";
    private static final String ROLE_PATTERN = "^(CN=.+,*)+$";
    private static final String CN = "CN";
    private static final List KNOWN_ATTRIBUTES = Arrays.asList(FIRSTNAME, LASTNAME, EMAIL,
            MEMBEROF, COMPANY);
    private static final long SECONDS_IN_A_DAY = 86400L;

    private Utils() {
    }


    /**
     * Extract user attributes from XMLObject.
     *
     * @param name       the username
     * @param attributes the saml attribute list
     * @param expiresOn  expiration period of the JWTAuthToken @{@link JWTAuthToken}
     * @return JWTAuthToken @{@link JWTAuthToken}
     */
    public static JWTAuthToken buildAttributes(String name, List attributes,
                                               long expiresOn, String sessionIndex) {
        JWTAuthToken token = buildJWTAuthToken(new JWTAuthToken(name, name), attributes, expiresOn);
        token.setSessionIndex(sessionIndex);
        return token;
    }

    /**
     * Build Attributes from name, credential and attribute list.
     *
     * @param name        username
     * @param credentials SAMLCredential
     * @param attributes  saml attribute list
     * @param expiresOn   expiration
     * @return JWTAuthToken
     */
    public static JWTAuthToken buildAttributes(String name, SAMLCredential credentials,
                                               List attributes, long expiresOn) {
        return buildJWTAuthToken(new JWTAuthToken(name, credentials), attributes, expiresOn);
    }

    /**
     * Build JWTAuthToken based on username, and saml assertion attribute list.
     *
     * @param attributes   the attributes
     * @param expiresOn    expiration
     * @param jwtAuthToken the authToken
     * @return JWTAuthToken
     */
    public static JWTAuthToken buildJWTAuthToken(JWTAuthToken jwtAuthToken, List attributes, long expiresOn) {
        Map<String, List<String>> attributeMap = new HashMap<>();
        List<Object> roles = new ArrayList<>();
        for (Object attribute : attributes) {
            for (XMLObject attr : ((AttributeImpl) attribute).getAttributeValues()) {
                populateName(attr, attribute, jwtAuthToken);
                populateEmail(attr, attribute, jwtAuthToken);
                populateCompany(attr, attribute, jwtAuthToken);
                populateRoles(attr, attribute, roles);
                populateAttributes(attr, attribute, attributeMap);
            }
        }
        jwtAuthToken.setName(jwtAuthToken.getPrincipal().toString());
        jwtAuthToken.setAuthorities(getSimpleGrantedAuthorities(roles));
        jwtAuthToken.setAttributes(attributeMap);
        jwtAuthToken.setExpiresOn(expiresOn);
        return jwtAuthToken;
    }

    /**
     * Return Optional.empty() for nested calls when there is a npe.
     *
     * @param resolver the supplier
     * @param <T>      the type
     */
    public static <T> Optional<T> resolve(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * Parses the SAMLCredential for expiration time. Locates all AuthnStatements present
     * within the assertion (only one in most cases) and computes the expiration
     * based on sessionNotOnOrAfter.
     * field.
     *
     * @param statementList credential to use for expiration parsing.
     * @return Compute expiration from sessionNotOnOrAfter field, 86400L if not present.
     */
    public static long getExpiration(List<AuthnStatement> statementList) {
        DateTime expiration = null;
        for (AuthnStatement statement : statementList) {
            expiration = statement.getSessionNotOnOrAfter();
            if (expiration != null) {
                return expiration.getMillis() / 1000;
            }
        }
        return SECONDS_IN_A_DAY;
    }

    /**
     * Given a date return duration between current time and the date in seconds.
     *
     * @param expiration expiration
     * @return duration
     */
    public static long getExpiration(Long expiration) {
        return (expiration - DateTime.now().getMillis() / 1000);

    }

    /**
     * Parses the SAMLCredential for sessionIndex. Locates all AuthnStatements present
     * within the assertion (only one in most cases) and picks the sessionIndex.
     *
     * @param statementList AuthnStatements from SAMLCredential
     * @return SessionIndex if present, else empty string
     */
    public static String getSessionIndex(List<AuthnStatement> statementList) {
        String sessionIndex = null;
        for (AuthnStatement statement : statementList) {
            sessionIndex = statement.getSessionIndex();
            if (sessionIndex != null) {
                return sessionIndex;
            }
        }
        return "";
    }

    /**
     * Returns granted authorities from a list of roles.
     *
     * @param authorities authorities
     * @return simpleGrantedAuthorities
     */

    public static List<SimpleGrantedAuthority> getSimpleGrantedAuthorities(List<Object> authorities) {
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        if (authorities != null && !authorities.isEmpty()) {
            authorities.forEach(
                    authority -> simpleGrantedAuthorities
                            .add(new SimpleGrantedAuthority(authority.toString())));
        }
        return simpleGrantedAuthorities;
    }


    /**
     * Populate user first name and last name.
     *
     * @param attr         attribute values of an Attribute
     * @param attribute    Attributes in SAML Assertion
     * @param jwtAuthToken @{@link JWTAuthToken} AUthentication Object
     */
    private static void populateName(XMLObject attr, Object attribute, JWTAuthToken jwtAuthToken) {
        if (((AttributeImpl) attribute).getName().equals(FIRSTNAME)) {
            jwtAuthToken.setFirstName(((XSStringImpl) attr).getValue());
        } else if (((AttributeImpl) attribute).getName().equals(LASTNAME)) {
            jwtAuthToken.setLastName(((XSStringImpl) attr).getValue());
        }
    }

    /**
     * Populate Organization of the user.
     *
     * @param attr         attribute values of an Attribute
     * @param attribute    Attributes in SAML Assertion
     * @param jwtAuthToken @{@link JWTAuthToken} AUthentication Object
     */
    private static void populateCompany(XMLObject attr, Object attribute, JWTAuthToken jwtAuthToken) {
        if (((AttributeImpl) attribute).getName().equals(COMPANY)) {
            jwtAuthToken.setOrganization(((XSStringImpl) attr).getValue());
        }
    }

    /**
     * Populate user email.
     *
     * @param attr         attribute values of an Attribute
     * @param attribute    Attributes in SAML Assertion
     * @param jwtAuthToken @{@link JWTAuthToken} AUthentication Object
     */
    private static void populateEmail(XMLObject attr, Object attribute, JWTAuthToken jwtAuthToken) {
        if (((AttributeImpl) attribute).getName().equals(EMAIL)) {
            jwtAuthToken.setEmail(((XSStringImpl) attr).getValue());
        }
    }

    /**
     * Populate user roles.
     *
     * @param attr      attribute values of an Attribute
     * @param attribute Attributes in SAML Assertion
     * @param roles     SAML user roles
     */
    private static void populateRoles(XMLObject attr, Object attribute, List roles) {
        if (((AttributeImpl) attribute).getName().equals(MEMBEROF)
                && ((XSStringImpl) attr).getValue() != null) {
            String role = ((XSStringImpl) attr).getValue();
            if (Pattern.compile(ROLE_PATTERN).matcher(role).matches()) {
                roles.add(refineRole(role));
            }
        }
    }

    /**
     * q
     * Parse the memberOf and fetches only the attribute of CN .
     * <p>
     * Eg: "CN=aseadmin,CN=Users,DC=imagineaoh,DC=com"  parses the string and fetch role: "aseadmin"
     *
     * @param memberOf a role value of the memberOf attribute
     * @return role of the parsed attribute memberOf.
     */
    private static String refineRole(String memberOf) {
        String[] allRoles = memberOf.split(AuthServerConstants.CHAR_COMMA);
        String[] cnRole = allRoles[0].split(AuthServerConstants.CHAR_EQUAL);
        return cnRole[1];
    }

    /**
     * Populate other user attributes into attribute map.
     *
     * @param attr         attribute values of an Attribute
     * @param attribute    Attributes in SAML Assertion
     * @param attributeMap AttributeMap holding other SAML attributes
     */
    private static void populateAttributes(XMLObject attr, Object attribute, Map<String, List<String>> attributeMap) {
        if (!KNOWN_ATTRIBUTES.contains(((AttributeImpl) attribute).getName())) {
            if (attributeMap.get(((AttributeImpl) attribute).getName()) == null) {
                attributeMap.put(((AttributeImpl) attribute).getName(), new ArrayList<>());
            }
            attributeMap.get(((AttributeImpl) attribute).getName())
                    .add(((XSStringImpl) attr).getValue());
        }
    }
}



