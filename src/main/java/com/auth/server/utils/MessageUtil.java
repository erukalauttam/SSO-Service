package com.auth.server.utils;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Message Utility to fetch message corresponding to a code from resource bundle.
 */

public class MessageUtil {

  private static ReloadableResourceBundleMessageSource messageSource;
  private static final int CACHE_SECONDS = 3600;

  static {
    messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:messages/auth-messages");
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setCacheSeconds(CACHE_SECONDS);
  }

  private MessageUtil(){}

  /**
   * Get message from resourcebundle.
   *
   * @param message the message
   * @param args the args for dynamic values
   * @return the message
   */
  public static String getMessage(String message, Object...args) {
    return messageSource.getMessage(message, args, LocaleContextHolder.getLocale());
  }
}
