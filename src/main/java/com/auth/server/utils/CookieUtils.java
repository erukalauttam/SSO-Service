package com.auth.server.utils;

import com.auth.server.AuthServerConstants;
import com.nimbusds.jose.JWSObject;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Utils for handling Http Cookies.
 *
 * @link javax.servlet.http.Cookie
 */

public class CookieUtils {

  private CookieUtils() { }

  /**
   * Extract JWT if present from cookie or the header from request.
   *
   * @param request the request
   * @return JWT the token
   */
  public static String extractJWTFromCookieOrHeader(HttpServletRequest request) {
    if (request.getCookies() != null) {
      for (Cookie cookie : request.getCookies()) {
        if (cookie.getName().equals(AuthServerConstants.ONELOGIN_COOKIE_NAME)) {
          return cookie.getValue();
        }
      }
    }
    return request.getHeader(AuthServerConstants.ONELOGIN_COOKIE_NAME);
  }

  /**
   * Add cookie to response with jwt on successful authentication.
   *
   * @param jwsObject  the jwsObject @{@link JWSObject} Object
   * @param response   the response @{@link HttpServletResponse}
   * @param expiration expiration time of the JWT cookie.
   */
  public static void addJWTToCookie(JWSObject jwsObject,
                                    HttpServletResponse response, long expiration) {

    Cookie cookie = new Cookie(AuthServerConstants.ONELOGIN_COOKIE_NAME, jwsObject.serialize());
    cookie.setPath("/");
    cookie.setSecure(true);
    cookie.setHttpOnly(true);
    cookie.setMaxAge(Long.valueOf(expiration).intValue());
    response.addCookie(cookie);
  }
}
