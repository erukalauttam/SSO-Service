package com.auth.server.exception;

/**
 * Thrown when there are issues contacting the token store. (i.e Redis Cache).
 */
public class DataStoreUnavailableException extends AuthException {

  /**
   * DataStoreUnavailableException.
   * @see AuthException#AuthException(String, Object...)
   */
  public DataStoreUnavailableException(String errorCode, Object... args) {
    super(errorCode, args);
  }
}
