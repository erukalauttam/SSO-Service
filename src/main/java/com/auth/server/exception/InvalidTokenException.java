package com.auth.server.exception;

/**
 * Thrown when token is found invalid during verification.
 */
public class InvalidTokenException extends AuthException {

  /**
   * (non-Javadoc).
   *
   */
  public InvalidTokenException(String errorCode, Object... args) {
    super(errorCode, args);
  }
}
