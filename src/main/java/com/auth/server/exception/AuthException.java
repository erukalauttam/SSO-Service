package com.auth.server.exception;

import com.auth.server.utils.MessageUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;

/**
 * Generic Authentication Exception class.
 */

public class AuthException extends AuthenticationException {

  @Getter
  private String errorCode = null;

  @Getter
  private String message = null;

  @Getter @Setter
  private Object[] args = null;

  @Getter
  @Deprecated
  private HttpStatus statusCode;


  /**
   * Construct a new AuthException with errorCode and arguments. The error code and args are
   * converted into message and set in the exception
   * @param errorCode errorCode to be converted and set as an understandable exception message
   * @param args the args to replace the placeholders in message with dynamic values.
   */
  public AuthException(String errorCode, Object... args) {
    super(MessageUtil.getMessage(errorCode, args));
    this.errorCode = errorCode;
    this.message = MessageUtil.getMessage(errorCode, args);
    this.args = args;
  }

  /**
   * Construct a new AuthException with status code, errorCode and arguments.
   * @param statusCode HttpStatus code for the exception.
   * @param errorCode  errorCode to be converted and set as an understandable exception message.
   * @param args the args to replace the placeholders in message with dynamic values.
   */
  public AuthException(HttpStatus statusCode, String errorCode, Object... args) {
    super(MessageUtil.getMessage(errorCode, args));
    this.errorCode = errorCode;
    this.message = MessageUtil.getMessage(errorCode, args);
    this.args = args;
    this.statusCode = statusCode;
  }

  /**
   * Construct a new AuthException with status code, errorCode, exception cause and arguments.
   * @param errorCode  HttpStatus code for the exception.
   * @param args  the args to replace the placeholders in message with dynamic values.
   */
  public AuthException(String errorCode, Throwable cause, Object... args) {
    super(MessageUtil.getMessage(errorCode, args), cause);
    this.errorCode = errorCode;
    this.message = MessageUtil.getMessage(errorCode, args);
    this.args = args;
  }

  /**
   * Constructs a new AuthException with the specified exception and a
   * detail message of <tt>(exception==null ? null : exception.toString())</tt>
   * (which typically contains the class and detail message of
   * <tt>exception</tt>).
   *
   * @param  cause the exception A <tt>null</tt> value is
   *         permitted, and indicates that the exception is nonexistent or
   *         unknown.
   */
  public AuthException(HttpStatus statusCode, Throwable cause) {
    super(cause.getMessage(), cause);
    this.message = cause.getMessage();
    this.statusCode = statusCode;
  }

  @Override
  public String toString() {
    return message;
  }

}


